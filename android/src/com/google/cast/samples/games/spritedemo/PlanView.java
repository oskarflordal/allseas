package com.google.cast.samples.games.spritedemo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Draw the action plan as a set of connected points and the resulting bezier surface
 */
class PlanView extends SurfaceView implements SurfaceHolder.Callback {

    class Segment {
        float x0, x1, y0, y1;

        Segment(float x0, float y0, float x1, float y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }
    }

    class PlanThread extends Thread {

        private Bitmap background;
        private Context context;

        private int canvasWidth;
        private int canvasHeight;

        /** Paint to draw the lines on screen. */
        private Paint linePaint;

        private BlockingQueue drawQueue;

        private ArrayList<Segment> lastDrawn;
        private ArrayList<Segment> preLastDrawn;

        /** Handle to the surface manager object we interact with */
        private SurfaceHolder surfaceHolder;

        public PlanThread(SurfaceHolder surfaceHolder, Context context) {
            this.surfaceHolder = surfaceHolder;
            this.context = context;

            lastDrawn = new ArrayList<>(16);
            preLastDrawn = new ArrayList<>(16); // hehe :)
            drawQueue = new ArrayBlockingQueue(128);

            Resources res = context.getResources();

            // background grid
//            backgroundImage = BitmapFactory.decodeResource(res, R.drawable.earthrise);

            linePaint = new Paint();
            linePaint.setAntiAlias(true);
            linePaint.setARGB(255, 255, 0, 0);
            linePaint.setStrokeWidth(10.0f);
        }

        /**
         * pause the thread
         */
        public void pause() {
            synchronized (surfaceHolder) {
            }
        }

        /**
         * Restore the game on app restart, not sure how well we will handle this
         * TODO
         *
         */
        public synchronized void restoreState(Bundle savedState) {
            synchronized (surfaceHolder) {
            }
        }

        @Override
        public void run() {
            while (true) {
                Canvas c = null;

                try {
                    // grab the current vancas
                    c = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        doDraw(c);
                    }
                } finally {
                    // do this in a finally so that if an exception is thrown
                    // during the above, we don't leave the Surface in an
                    // inconsistent state
                    if (c != null) {
                        // TODO: I guess we can
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        /**
         * Dump the game state when activity is pausing
         */
        public Bundle saveState(Bundle map) {
            synchronized (surfaceHolder) {
            }
            return map;
        }

        /* Callback invoked when the surface dimensions change. */
        public void setSurfaceSize(int width, int height) {
            // synchronized to make sure these all change atomically
            synchronized (surfaceHolder) {
                canvasWidth = width;
                canvasHeight = height;

                // don't forget to resize the background image
//                background = Bitmap.createScaledBitmap(
//                        background, width, height, true);
            }
        }

        /**
         * Resumes from a pause.
         */
        public void unpause() {
            // Move the real time clock up to now
            synchronized (surfaceHolder) {
            }
        }

        private int tick;

        /**
         * Draws the ship, fuel/speed bars, and background to the provided
         * Canvas.
         */
        private void doDraw(Canvas canvas) {
            // note that drwing a bg would clear, then we need to accumulate to another buffer and blit
//            canvas.drawBitmap(backgroundImage, 0, 0, null);

            // avoid double/tripple buffering issues by redrawing everying from last frame
            // the official way to do this is to draw to a private buffer and then blit, but whats the fun in that!
            for (Segment srep : preLastDrawn) {
                canvas.drawLine(srep.x0, srep.y0, srep.x1, srep.y1, linePaint);
            }

            preLastDrawn.clear();
            preLastDrawn.addAll(lastDrawn);

            for (Segment srep : lastDrawn) {
                canvas.drawLine(srep.x0, srep.y0, srep.x1, srep.y1, linePaint);
            }

            Segment s = (Segment) drawQueue.poll();

            lastDrawn.clear();

            while (s != null) {
                lastDrawn.add(s);

                canvas.drawLine(s.x0, s.y0, s.x1, s.y1, linePaint);
                s = (Segment) drawQueue.poll();
            }

            // TODO needed?
            canvas.save();
        }

        public void addSegment(Segment s) {
            drawQueue.add(s);
        }
    }

    private PlanThread thread;

    public PlanView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        // create thread only; it's started in surfaceCreated()
        thread = new PlanThread(holder, context);

        setFocusable(true); // make sure we get key events
    }

    /**
     */
    public PlanThread getThread() {
        return thread;
    }


    /**
     * Standard window-focus override. Notice focus lost so we can pause on
     * focus lost. e.g. user switches to take a call.
     */
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) thread.pause();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        thread.setSurfaceSize(width, height);
    }

    private float prevTouchX;
    private float prevTouchY;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        switch(event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                prevTouchX = event.getX();
                prevTouchY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                thread.addSegment(new Segment(prevTouchX, prevTouchY, event.getX(), event.getY()));
                break;
            case MotionEvent.ACTION_MOVE:
                thread.addSegment(new Segment(prevTouchX, prevTouchY, event.getX(), event.getY()));
                prevTouchX = event.getX();
                prevTouchY = event.getY();
                break;
            default: Log.e("GOO", "other" + event.getAction()); break;
        }

        return true;
    }

    /*
         * Callback invoked when the Surface has been created and is ready to be
         * used.
         */
    public void surfaceCreated(SurfaceHolder holder) {
        Log.e("GOOO", "STARTING!");

        thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }
}