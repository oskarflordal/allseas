
// game state object
var game;

function preload() {
    game.load.image('boat', 'boat.png');
}

function create() {
}

function render () {
}

function start() {
    game = new Phaser.Game(1024, 1024, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, render: render });
}



window.onload = function() {
    start();
}

